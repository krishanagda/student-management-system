import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;
import java.sql.SQLException;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Krisha B Nagda
 */

public class MySQLConnect {
    /*
        * The below method creates a connection with mysql database named as 'student_mgmt' with user account named 'krishanagda' whose password is 'krishanagda'.
        * It returns the java.sql.connection object if the connection was successfully established otherwise returns 'null'
    */
    public static Connection getConnection() {
        try{
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/student_mgmt","krishanagda","krishanagda");
            return conn;
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null,"Connection failed "+ e.getMessage());
            return null;
        }
    }
    
//    public static void main(String args[]){
//        Connection conn = getConnection();
//        if(conn!=null){
//            JOptionPane.showMessageDialog(null,"Connection Established");
//        }
//    }
}
